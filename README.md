# pytestpippack
A minimal homemade python package for testing importing

## Python Modules
Modules in Python are simply Python files with a .py extension. The name of the module will be the name of the file. A Python module can have a set of functions, classes or variables defined and implemented.

## Python Packages
Packages are namespaces which contain multiple packages and modules themselves. They are simply directories, but with a twist.

Each package in Python is a directory which MUST contain a special file called `__init__.py`. This file can be empty, and it indicates that the directory it contains is a Python package, so it can be imported the same way a module can be imported.

If we create a directory called `foo`, which marks the package name, we can then create a module inside that package called `bar`. We also must not forget to add the `__init__.py` file inside the `foo` directory.

## Packaging and make it pip'able

https://python-packaging.readthedocs.io/en/latest/minimal.html

## Installation example - pip from URL / GIT
```
!pip install --upgrade git+https://github.com/willemhendriks/pytestpippack
```
## Installation example - pip from .zip / .tar.gz
```
pip install --upgrade path/to/file/pytestpippack-master.zip
```

## Import and execute to test

```
In [8]: from pytestpippack import mymodule

In [9]: mymodule.my_function()
Hello, is it this function you are looking for?
Out[9]: 0.0
```
