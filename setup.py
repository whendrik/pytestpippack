from setuptools import setup

setup(name='pytestpippack',
      version='0.2',
      description='Minimal Structure needed for PIP install homemade stuff',
      url='https://github.com/willemhendriks/pytestpippack/',
      author='Willem Hendriks',
      author_email='whendrik@gmail.com',
      license='MIT',
      packages=['pytestpippack'],
      zip_safe=False)
